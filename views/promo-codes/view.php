<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PromoCode */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Промо-коды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-code-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот промо-код?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'date_start',
            'date_end',
            ['attribute' => 'amount', 'value' => $model->rub_amount],
            ['attribute' => 'tariff_zone_name', 'value' => $model->tariff_zone_name],
            ['attribute' => 'status', 'value' => $model->status_name],
        ],
    ]) ?>

</div>
