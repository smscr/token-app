<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PromoCode */

$this->title = 'Создать промо-код';
$this->params['breadcrumbs'][] = ['label' => 'Промо-коды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-code-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
