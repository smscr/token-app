<?php

use app\models\Cities;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use nex\datepicker\DatePicker;

$disableEdit = isset($model->status) && !(bool) $model->status;
$datesOptions = ['clientOptions' => ['format' => 'Y-MM-DD']];

/* @var $this yii\web\View */
/* @var $model app\models\PromoCode */
/* @var $cities app\models\Cities[] */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-code-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => $disableEdit]) ?>

    <?= $disableEdit
        ? $form->field($model, 'date_start')->textInput(['disabled' => $disableEdit])
        : $form->field($model, 'date_start')->widget(DatePicker::className(), $datesOptions)
    ?>

    <?= $disableEdit
        ? $form->field($model, 'date_end')->textInput(['disabled' => $disableEdit])
        : $form->field($model, 'date_end')->widget(DatePicker::className(), $datesOptions)
    ?>

    <?= $form
        ->field($model, 'amount', ['template' => "{label}, руб.\n{input}\n{hint}\n{error}"])
        ->input('number', ['disabled' => $disableEdit])
    ?>

    <?= $form->field($model, 'tariff_zone')->dropDownList(ArrayHelper::map(Cities::find()->all(), 'id', 'name'), ['disabled' => $disableEdit]) ?>

    <?php if (!$model->isNewRecord):?>
        <?= $form->field($model, 'status')->dropDownList(['Неактивный', 'Активный']) ?>
    <?php endif;?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
