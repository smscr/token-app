<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cities`.
 */
class m171225_000614_create_cities_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cities', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
        
        $this->batchInsert('cities', ['name'], [
            ['Москва'], ['Санкт-Петербург'], ['Новосибирск'], ['Екатеринбург'], ['Нижний Новгород']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cities');
    }
}
