<?php

use yii\db\Migration;

/**
 * Handles the creation of table `promo_code`.
 * Has foreign keys to the tables:
 *
 * - `cities`
 */
class m171225_002038_create_promo_code_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('promo_code', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'date_start' => $this->date()->notNull(),
            'date_end' => $this->date()->notNull(),
            'amount' => $this->integer()->notNull(),
            'tariff_zone' => $this->integer()->notNull(),
            'status' => $this->boolean()->notNull()->defaultValue(0),
        ]);

        // creates index for column `tariff_zone`
        $this->createIndex(
            'idx-promo_code-tariff_zone',
            'promo_code',
            'tariff_zone'
        );

        // add foreign key for table `cities`
        $this->addForeignKey(
            'fk-promo_code-tariff_zone',
            'promo_code',
            'tariff_zone',
            'cities',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `cities`
        $this->dropForeignKey(
            'fk-promo_code-tariff_zone',
            'promo_code'
        );

        // drops index for column `tariff_zone`
        $this->dropIndex(
            'idx-promo_code-tariff_zone',
            'promo_code'
        );

        $this->dropTable('promo_code');
    }
}
