/**
 * @api {get} /index.php?r=api/promo-search&name=:name Поиск промо-кода
 * @apiName SearchPromo
 * @apiGroup Promo
 *
 * @apiParam {String} name Название промо-кода
 * @apiParamExample {json} Request-Example:
                 GET /index.php?r=api/promo-search&name=Test
 * 
 * @apiHeader {String} Authorization Токен Bearer-авторизации
 * @apiHeader {String} Content-Type Тип данных обмена
 * @apiHeaderExample {String} Header-Example:
                 Authorization: Bearer 100-token
                 Content-Type: application/json
 *
 * @apiSuccess {Integer} amount Вознаграждение клиента
 *
 * @apiSuccess {String} date_start Дата начала действия промо-кода
 * @apiSuccess {String} date_end Дата завершения действия промо-кода
 * @apiSuccess {Integer} amount Вознаграждение клиента
 * @apiSuccess {String} tariff_zone_name Тарифная зона
 * @apiSuccess {Integer} status Статус промо-кода (1 - активен, 0 - неактивен)
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "date_start": "2017-12-06",
 *         "date_end": "2017-12-06",
 *         "amount": 5,
 *         "tariff_zone_name": "Екатеринбург",
 *         "status": 1
 *     }
 *
 * @apiError NotFoundHttpException Промо-код с таким названием не был найден
 *
 * @apiErrorExample Not Found:
 *     HTTP/1.1 404 Not Found
 *     {
 *         "name": "Not Found",
 *         "message": "Не найдено объекта, удовлетворяющего условиям поиска",
 *         "code": 0,
 *         "status": 404,
 *         "type": "yii\\web\\NotFoundHttpException"
 *     }
 *     
 * @apiError BadRequestHttpException Не указан name для поиска
 *
 * @apiErrorExample Bad Request:
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "name": "Bad Request",
 *         "message": "Отсутствует обязательный параметр: name",
 *         "code": 0,
 *         "status": 400,
 *         "type": "yii\\web\\BadRequestHttpException"
 *     }
 *     
 * @apiError UnauthorizedHttpException Не авторизовано
 *
 * @apiErrorExample Unauthorized:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *         "name": "Unauthorized",
 *         "message": "Your request was made with invalid credentials.",
 *         "code": 0,
 *         "status": 401,
 *         "type": "yii\\web\\UnauthorizedHttpException"
 *     }
 */