/**
 * @api {post} /index.php?r=api/promo-activate Активация промо-кода
 * @apiName ActivatePromo
 * @apiGroup Promo
 *
 * @apiParam {String} name Название промо-кода
 * @apiParam {String} tariff_zone_name Тарифная зона
 * @apiParamExample {json} Request-Example:
                 { "name": "Huge", "tariff_zone_name": "Москва" }
 * 
 * @apiHeader {String} Authorization Токен Bearer-авторизации
 * @apiHeader {String} Content-Type Тип данных обмена
 * @apiHeaderExample {String} Header-Example:
                 Authorization: Bearer 100-token
                 Content-Type: application/json
 *
 * @apiSuccess {Integer} amount Вознаграждение клиента
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "amount": 3432
 *     }
 *
 * @apiError UnprocessableEntityHttpException Промо-код с таким названием уже активирован
 *
 * @apiErrorExample Unprocessable entity:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *         "name": "Unprocessable entity",
 *         "message": "Промо-код уже активен!",
 *         "code": 0,
 *         "status": 422,
 *         "type": "yii\\web\\UnprocessableEntityHttpException"
 *     }
 *     
 * @apiError NotFoundHttpException Промо-код с такими названием и тарифной зоной не был найден
 *
 * @apiErrorExample Not Found:
 *     HTTP/1.1 404 Not Found
 *     {
 *         "name": "Not Found",
 *         "message": "Не найдено объекта, удовлетворяющего условиям поиска",
 *         "code": 0,
 *         "status": 404,
 *         "type": "yii\\web\\NotFoundHttpException"
 *     }
 *     
 * @apiError BadRequestHttpException Не указан name для поиска
 *
 * @apiErrorExample Bad Request (name):
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "name": "Bad Request",
 *         "message": "Отсутствует обязательный параметр: name",
 *         "code": 0,
 *         "status": 400,
 *         "type": "yii\\web\\BadRequestHttpException"
 *     }
 *     
 * @apiError BadRequestHttpException Не указана тарифная зона для поиска
 *
 * @apiErrorExample Bad Request (tariff_zone_name):
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "name": "Bad Request",
 *         "message": "Отсутствует обязательный параметр: tariff_zone_name",
 *         "code": 0,
 *         "status": 400,
 *         "type": "yii\\web\\BadRequestHttpException"
 *     }
 *     
 * @apiError UnauthorizedHttpException Не авторизовано
 *
 * @apiErrorExample Unauthorized:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *         "name": "Unauthorized",
 *         "message": "Your request was made with invalid credentials.",
 *         "code": 0,
 *         "status": 401,
 *         "type": "yii\\web\\UnauthorizedHttpException"
 *     }
 */