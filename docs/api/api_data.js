define({ "api": [
  {
    "type": "post",
    "url": "/index.php?r=api/promo-activate",
    "title": "Активация промо-кода",
    "name": "ActivatePromo",
    "group": "Promo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Название промо-кода</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tariff_zone_name",
            "description": "<p>Тарифная зона</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{ \"name\": \"Huge\", \"tariff_zone_name\": \"Москва\" }",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Токен Bearer-авторизации</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Тип данных обмена</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "Authorization: Bearer 100-token\nContent-Type: application/json",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "amount",
            "description": "<p>Вознаграждение клиента</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"amount\": 3432\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntityHttpException",
            "description": "<p>Промо-код с таким названием уже активирован</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFoundHttpException",
            "description": "<p>Промо-код с такими названием и тарифной зоной не был найден</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "BadRequestHttpException",
            "description": "<p>Не указан name для поиска</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedHttpException",
            "description": "<p>Не авторизовано</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Unprocessable entity:",
          "content": "HTTP/1.1 422 Unprocessable entity\n{\n    \"name\": \"Unprocessable entity\",\n    \"message\": \"Промо-код уже активен!\",\n    \"code\": 0,\n    \"status\": 422,\n    \"type\": \"yii\\\\web\\\\UnprocessableEntityHttpException\"\n}",
          "type": "json"
        },
        {
          "title": "Not Found:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"name\": \"Not Found\",\n    \"message\": \"Не найдено объекта, удовлетворяющего условиям поиска\",\n    \"code\": 0,\n    \"status\": 404,\n    \"type\": \"yii\\\\web\\\\NotFoundHttpException\"\n}",
          "type": "json"
        },
        {
          "title": "Bad Request (name):",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"name\": \"Bad Request\",\n    \"message\": \"Отсутствует обязательный параметр: name\",\n    \"code\": 0,\n    \"status\": 400,\n    \"type\": \"yii\\\\web\\\\BadRequestHttpException\"\n}",
          "type": "json"
        },
        {
          "title": "Bad Request (tariff_zone_name):",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"name\": \"Bad Request\",\n    \"message\": \"Отсутствует обязательный параметр: tariff_zone_name\",\n    \"code\": 0,\n    \"status\": 400,\n    \"type\": \"yii\\\\web\\\\BadRequestHttpException\"\n}",
          "type": "json"
        },
        {
          "title": "Unauthorized:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"name\": \"Unauthorized\",\n    \"message\": \"Your request was made with invalid credentials.\",\n    \"code\": 0,\n    \"status\": 401,\n    \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "docs/api/src/promo-activate.js",
    "groupTitle": "Promo"
  },
  {
    "type": "get",
    "url": "/index.php?r=api/promo-search&name=:name",
    "title": "Поиск промо-кода",
    "name": "SearchPromo",
    "group": "Promo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Название промо-кода</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "GET /index.php?r=api/promo-search&name=Test",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Токен Bearer-авторизации</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>Тип данных обмена</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "Authorization: Bearer 100-token\nContent-Type: application/json",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "amount",
            "description": "<p>Вознаграждение клиента</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "date_start",
            "description": "<p>Дата начала действия промо-кода</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "date_end",
            "description": "<p>Дата завершения действия промо-кода</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "tariff_zone_name",
            "description": "<p>Тарифная зона</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Статус промо-кода (1 - активен, 0 - неактивен)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"date_start\": \"2017-12-06\",\n    \"date_end\": \"2017-12-06\",\n    \"amount\": 5,\n    \"tariff_zone_name\": \"Екатеринбург\",\n    \"status\": 1\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFoundHttpException",
            "description": "<p>Промо-код с таким названием не был найден</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "BadRequestHttpException",
            "description": "<p>Не указан name для поиска</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnauthorizedHttpException",
            "description": "<p>Не авторизовано</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Not Found:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"name\": \"Not Found\",\n    \"message\": \"Не найдено объекта, удовлетворяющего условиям поиска\",\n    \"code\": 0,\n    \"status\": 404,\n    \"type\": \"yii\\\\web\\\\NotFoundHttpException\"\n}",
          "type": "json"
        },
        {
          "title": "Bad Request:",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"name\": \"Bad Request\",\n    \"message\": \"Отсутствует обязательный параметр: name\",\n    \"code\": 0,\n    \"status\": 400,\n    \"type\": \"yii\\\\web\\\\BadRequestHttpException\"\n}",
          "type": "json"
        },
        {
          "title": "Unauthorized:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n    \"name\": \"Unauthorized\",\n    \"message\": \"Your request was made with invalid credentials.\",\n    \"code\": 0,\n    \"status\": 401,\n    \"type\": \"yii\\\\web\\\\UnauthorizedHttpException\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "docs/api/src/promo-search.js",
    "groupTitle": "Promo"
  }
] });
