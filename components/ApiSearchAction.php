<?php

namespace app\components;

use Yii;

class ApiSearchAction extends \yii\rest\Action
{
    /**
     * @var string the field of the model which will be used for search
     */
    public $searchField;
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if ($this->searchField === null || !(is_string($this->searchField) || is_array($this->searchField))) {
            throw new \yii\base\InvalidConfigException(get_class($this) . '::$searchField must be set and be a string or an array.');
        }
    }

    /**
     * Searches and displays a model.
     * @return \yii\db\ActiveRecordInterface the model being displayed
     * @throws \yii\base\InvalidParamException if some required parameter not specified
     */
    public function run()
    {
        if (Yii::$app->request->method === 'GET') {
            $data = Yii::$app->request->get();
        } else {
            $data = Yii::$app->request->getBodyParams();
        }
        
        $params = is_array($this->searchField) ? $this->searchField : [$this->searchField];
        $wheres = [];
        foreach ($params as $attribute => $param) {
            if (is_int($attribute)) {
                $attribute = $param;
            }
            
            if (isset($data[$param])) {
                $wheres[$attribute] = $data[$param];
            } else {
                throw new \yii\web\BadRequestHttpException("Отсутствует обязательный параметр: {$param}");
            }
        }
        
        $model = $this->searchModel($wheres);
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        return $model;
    }
    
    /**
     * Returns the data model based on the field value given.
     * If the data model is not found, a 404 HTTP exception will be raised.
     * @param array $wheres conditions to be met on the model to be found
     * @return \yii\db\ActiveRecordInterface the model found
     * @throws \yii\web\NotFoundHttpException if the model cannot be found
     */
    public function searchModel($wheres)
    {
        /* @var $modelClass \yii\db\ActiveRecordInterface */
        $modelClass = $this->modelClass;
        $model = $modelClass::find()->where($wheres)->one();

        if (isset($model)) {
            return $model;
        }

        throw new \yii\web\NotFoundHttpException("Не найдено объекта, удовлетворяющего условиям поиска");
    }
}
