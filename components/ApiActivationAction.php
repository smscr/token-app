<?php

namespace app\components;

class ApiActivationAction extends ApiSearchAction
{
    /**
     * @var callable a method to activate the model
     * The method should return the result which will be sent to client
     */
    public $activationMethod;
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if ($this->activationMethod === null || !is_callable($this->activationMethod)) {
            throw new \yii\base\InvalidConfigException(get_class($this) . '::$activationMethod must be set and be callable.');
        }
    }

    /**
     * Finds and updates a model.
     * @return mixed the result of activation of the model
     */
    public function run()
    {
        // find the model
        $model = parent::run();

        return $this->activate($model);
    }
    
    /**
     * Finds and updates a model.
     * @param \yii\db\ActiveRecordInterface $model the model found by search
     * @return mixed
     */
    public function activate($model)
    {
        $method = $this->activationMethod;
        
        if (is_array($method) && is_a($model, $method[0])) {
            return $model->{$method[1]}();
        }
        
        return $method($model);
    }
}
