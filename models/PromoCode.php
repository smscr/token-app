<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promo_code".
 *
 * @property int $id
 * @property string $name
 * @property string $date_start
 * @property string $date_end
 * @property int $amount
 * @property int $tariff_zone
 * @property int $status
 *
 * @property Cities $tariffZone
 */
class PromoCode extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_start', 'date_end', 'amount', 'tariff_zone'], 'required'],
            [['date_start', 'date_end'], 'safe'],
            [['amount', 'tariff_zone', 'status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'match', 'pattern' => '/^[A-Za-z ]+$/i'],
            [['tariff_zone'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['tariff_zone' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название промо-кода',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'amount' => 'Вознаграждение клиента',
            'rub_amount' => 'Вознаграждение клиента',
            'tariff_zone' => 'Тарифная зона',
            'tariff_zone_search' => 'Тарифная зона',
            'status' => 'Статус',
            'status_name' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffZone()
    {
        return $this->hasOne(Cities::className(), ['id' => 'tariff_zone']);
    }
    
    /**
     * @return string amount in Russian rubles
     */
    public function getRub_amount()
    {
        return "{$this->amount} ₽";
    }
    
    /**
     * @return string descriptive name of promo-code status
     */
    public function getStatus_name()
    {
        switch ($this->status) {
            case self::STATUS_ACTIVE: return 'Активен';
            case self::STATUS_INACTIVE: return 'Неактивен';
        }
    }
    
    /**
     * @return string name of the tariff zone
     */
    public function getTariff_zone_name()
    {
        return $this->tariffZone->name;
    }
    
    /**
     * Activate the PromoCode, save the Model, and return its amount
     * @return int|mixed amount or whatever validation errors occured
     * @throws \yii\web\UnprocessableEntityHttpException if the promo-code is already active
     */
    public function activateAndGetAmount()
    {
        if ($this->status !== self::STATUS_INACTIVE) {
            throw new \yii\web\UnprocessableEntityHttpException("Промо-код уже активен!");
        }
        
        $this->status = self::STATUS_ACTIVE;
        $this->save();
        return ['amount' => $this->amount];
    }
}
