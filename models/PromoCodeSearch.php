<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PromoCode;

/**
 * PromoCodeSearch represents the model behind the search form of `app\models\PromoCode`.
 */
class PromoCodeSearch extends PromoCode
{
    /**
     * @var string search for tariff zone name
     */
    public $tariff_zone_search;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'status'], 'integer'],
            [['date_start', 'date_end', 'tariff_zone_search'], 'safe'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function fields()
    {
        return ['date_start', 'date_end', 'amount', 'tariff_zone_name', 'status'];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return parent::find()->joinWith('tariffZone');
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['tariff_zone_search'] = [
            'asc' => ['cities.name' => SORT_ASC],
            'desc' => ['cities.name' => SORT_DESC],
        ];
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'amount' => $this->amount,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'cities.name', $this->tariff_zone_search]);

        return $dataProvider;
    }
}
