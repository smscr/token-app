<?php

namespace app\controllers;

use app\models\PromoCode;

class ApiController extends \yii\rest\ActiveController
{
    /**
     * @var string PromoCode model class name
     */
    public $modelClass = 'app\models\PromoCodeSearch';
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['authMethods'][] = \yii\filters\auth\HttpBearerAuth::className();
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'promo-search' => [
                'class' => 'app\components\ApiSearchAction',
                'modelClass' => $this->modelClass,
                'searchField' => ['promo_code.name' => 'name'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'promo-activate' => [
                'class' => 'app\components\ApiActivationAction',
                'modelClass' => $this->modelClass,
                'searchField' => ['promo_code.name' => 'name', 'cities.name' => 'tariff_zone_name'],
                'checkAccess' => [$this, 'checkAccess'],
                'activationMethod' => [PromoCode::className(), 'activateAndGetAmount']
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'search' => ['GET', 'POST'],
            'activate' => ['POST']
        ];
    }

}
